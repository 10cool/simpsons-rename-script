import os

import fixfiles

def walk(dir, exclude):
    for root, dirs, files in os.walk(dir):
        dirs[:] = [d for d in dirs if d not in exclude]

        for file_name in files:
            new_file_name =  fixfiles.transform_one_digit_seasons_to_two(
                fixfiles.transform_add_missing_season(
                    fixfiles.transform_break_up_season_and_disc(
                        fixfiles.transform_disc_to_d(
                            fixfiles.transform_season_to_s(
                                fixfiles.transform_break_up_simpsons(file_name))))))

            if file_name != new_file_name:
                os.rename(
                    os.path.join(root, file_name),
                    os.path.join(root, new_file_name))



walk(os.path.dirname(
    os.path.abspath(__file__)),
    [
        '.git',
        'season01',
        'season02',
        'season03',
        'season04',
        'season05',
        'season06',
        'season07',
        'season08',
        'season09',
        'season10',
    ]
)
